package com.coop.day;

public class Constants {
    public static final Integer ERR_SUCCESS = 0;
    public static final Integer ERR_FAILURE = -1;
    public static final Integer ERR_INVALID_AFTER_DATE  = -100;

    public static final String ERR_MSG_SUCCESS  = "Success";
    public static final String ERR_MSG_FAILURE  = "Unexpected error";

    public static final String SUCCESS_RETURN  = "The next working day: ";
    public static final String ERROR_INVALID_AFTER_DATE  = "The after date is not a valid date";
    public static final String ERROR_UNEXPECTED  = "Unexpected error, please try again after some time.";

    public static final String DATE_FORMAT  = "dd/MM/yyyy";

    public static final String API_NEXT_WORKING_DAY = "/next-working-day";

    public static final String NEXT_WORKING_DAY_API_PARAM   = "after";

    public static final String NEXT_WORKING_DAY_HEADER_KEY_DESC   = "description";
    public static final String NEXT_WORKING_DAY_HEADER_KEY_DATE   = "date";
}
