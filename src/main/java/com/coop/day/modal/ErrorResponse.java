package com.coop.day.modal;

import com.coop.day.Constants;

//Error Class for success the error code is 0.
public class ErrorResponse {
    int errorCode;
    String errorMessage;

    public ErrorResponse () {
        errorCode   = Constants.ERR_SUCCESS;
        errorMessage = Constants.ERR_MSG_SUCCESS;
    }

    public ErrorResponse (Integer errorCode, String errorMessage) {
        this.errorCode   = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
