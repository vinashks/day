package com.coop.day.modal;

import java.time.LocalDate;

//The modal class which returns by the API includes the next date and the error string.
public class NextWorkingDay {
    LocalDate nextWorkingDay;
    ErrorResponse errorResponse;

    public NextWorkingDay () {

    }

    public LocalDate getNextWorkingDay() {
        return nextWorkingDay;
    }

    public void setNextWorkingDay(LocalDate nextWorkingDay) {
        this.nextWorkingDay = nextWorkingDay;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }
}
