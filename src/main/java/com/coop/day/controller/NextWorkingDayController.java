package com.coop.day.controller;

import com.coop.day.Constants;
import com.coop.day.error.DayException;
import com.coop.day.modal.ErrorResponse;
import com.coop.day.modal.NextWorkingDay;
import com.coop.day.service.NextWorkingDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Date;

@RestController
@RequestMapping(value = Constants.API_NEXT_WORKING_DAY)
//The controller class which exposes the API.
public class NextWorkingDayController {
    @Autowired
    NextWorkingDayService nextWorkingDayService;

    @GetMapping
    //Function exposes the Get REST API of the class.
    public ResponseEntity getNextWorkingDay (@RequestParam (value = Constants.NEXT_WORKING_DAY_API_PARAM, required = false) String after) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_JSON);

        NextWorkingDay nextWorkingDay = new NextWorkingDay();

        try {
            LocalDate nextWorkingDate = nextWorkingDayService.getNextWorkingDay(after);
            responseHeaders.add(Constants.NEXT_WORKING_DAY_HEADER_KEY_DESC, Constants.SUCCESS_RETURN);
//            responseHeaders.add(Constants.NEXT_WORKING_DAY_HEADER_KEY_DATE, nextWorkingDate.toString());

            nextWorkingDay.setNextWorkingDay(nextWorkingDate);
            nextWorkingDay.setErrorResponse(new ErrorResponse());
            //Return the NextWorkingDay object with next working day and error object.
            return new ResponseEntity<NextWorkingDay>(nextWorkingDay, responseHeaders, HttpStatus.OK);
        } catch (DayException exception) {
            //Handle any exception thrown by the application return corresponding message.
            responseHeaders.add(Constants.NEXT_WORKING_DAY_HEADER_KEY_DESC, Constants.ERROR_INVALID_AFTER_DATE);
            nextWorkingDay.setErrorResponse(new ErrorResponse(Constants.ERR_INVALID_AFTER_DATE, exception.getMessage()));
            return new ResponseEntity<NextWorkingDay>(nextWorkingDay, responseHeaders, HttpStatus.BAD_REQUEST);
        } catch (Exception exception) {
            //Handle any un-handled exception and return unexpected error. This is not likely to happen.
            responseHeaders.add(Constants.NEXT_WORKING_DAY_HEADER_KEY_DESC, Constants.ERROR_UNEXPECTED);
            nextWorkingDay.setErrorResponse(new ErrorResponse(Constants.ERR_FAILURE, Constants.ERR_MSG_FAILURE));
            return new ResponseEntity<NextWorkingDay>(nextWorkingDay, responseHeaders, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
