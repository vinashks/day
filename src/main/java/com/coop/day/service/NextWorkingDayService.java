package com.coop.day.service;

import com.coop.day.Constants;
import com.coop.day.error.DayException;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
//The service class to do the actual algorithm.
public class NextWorkingDayService {

    //Function which returns the next working day.
    public LocalDate getNextWorkingDay (String after) throws DayException{
        LocalDate afterDate = getAfterDate (after);
        if (null != afterDate) {
            LocalDate nextWorkingDay = getNextWorkingDay(afterDate);
            return  nextWorkingDay;
        } else {
            throw new DayException(Constants.ERROR_INVALID_AFTER_DATE);
        }
    }

    //If it is Friday or saturday then return the next Monday otherwise return the next day.
    private LocalDate getNextWorkingDay(LocalDate afterDate) {
        DayOfWeek dayOfWeek = afterDate.getDayOfWeek();
        switch (dayOfWeek) {
            case FRIDAY:
                return afterDate.plusDays(3);
            case SATURDAY:
                return afterDate.plusDays(2);
            default:
                return afterDate.plusDays(1);
        }
    }

    //Convert string to local date class.
    private LocalDate getAfterDate (String after) throws DayException {
        if (null == after) {
            return LocalDate.now();
        } else {
            try {
                isValidDate(after);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constants.DATE_FORMAT);
                return LocalDate.parse(after, formatter);
            } catch (Exception ex) {
                throw new DayException(Constants.ERROR_INVALID_AFTER_DATE);
            }
        }
    }

    //Validate the input using SimpleDateFormat class by setting the linient flag to false. If the date is not valid then
    //ParseException is thrown by the parse function of SimpleDateFormat class.
    private Boolean isValidDate (String date) throws DayException{
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
            sdf.setLenient(false);
            sdf.parse(date);
            return true;
        } catch (ParseException exception) {
            throw new DayException(Constants.ERROR_INVALID_AFTER_DATE);
        }
    }
}
