package com.coop.day.error;

public class DayException extends Exception {
    public DayException (String message) {
        super(message);
    }
}
