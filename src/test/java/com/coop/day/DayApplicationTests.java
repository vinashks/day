package com.coop.day;

import com.coop.day.controller.NextWorkingDayController;
import com.coop.day.modal.NextWorkingDay;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import org.junit.Assert;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class DayApplicationTests {
    MockMvc mockMvc;

    @Mock
    private NextWorkingDayController nextWorkingDayController;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(nextWorkingDayController).build();
    }

    @Test
    void testFriday() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=06/12/2019", NextWorkingDay.class);
        Assert.assertEquals(200, response.getStatusCode().value());

        NextWorkingDay nextWorkingDay = response.getBody();
        Assert.assertEquals(0, nextWorkingDay.getErrorResponse().getErrorCode());
        Assert.assertEquals(9, nextWorkingDay.getNextWorkingDay().getDayOfMonth());
    }

    @Test
    void testSaturday() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=07/12/2019", NextWorkingDay.class);
        Assert.assertEquals(200, response.getStatusCode().value());

        NextWorkingDay nextWorkingDay = response.getBody();
        Assert.assertEquals(0, nextWorkingDay.getErrorResponse().getErrorCode());
        Assert.assertEquals(9, nextWorkingDay.getNextWorkingDay().getDayOfMonth());
    }

    @Test
    void testSunday() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=08/12/2019", NextWorkingDay.class);
        Assert.assertEquals(200,response.getStatusCode().value());
        NextWorkingDay nextWorkingDay = response.getBody();
        Assert.assertEquals(0, nextWorkingDay.getErrorResponse().getErrorCode());
        Assert.assertEquals(9, nextWorkingDay.getNextWorkingDay().getDayOfMonth());
        //String nextWorkingDay = response.getHeaders().getFirst("Date");
        //Assert.assertNotNull(nextWorkingDay);
    }

    @Test
    void testMonday() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=09/12/2019", NextWorkingDay.class);
        Assert.assertEquals(200, response.getStatusCode().value());

        NextWorkingDay nextWorkingDay = response.getBody();
        Assert.assertEquals(0, nextWorkingDay.getErrorResponse().getErrorCode());
        Assert.assertEquals(10, nextWorkingDay.getNextWorkingDay().getDayOfMonth());
    }

    @Test
    void testEmptyAfter() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day", NextWorkingDay.class);
        Assert.assertEquals(200, response.getStatusCode().value());

        NextWorkingDay nextWorkingDay = response.getBody();
        Assert.assertEquals(0, nextWorkingDay.getErrorResponse().getErrorCode());
//        Assert.assertEquals(LocalDate.now().getDayOfMonth() + 1, nextWorkingDay.getNextWorkingDay().getDayOfMonth());
    }

    @Test
    void testInvalidDate() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=31/04/2019", NextWorkingDay.class);
        Assert.assertEquals(400, response.getStatusCode().value());
    }

    @Test
    void testInvalidDateAsString() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=abc", NextWorkingDay.class);
        Assert.assertEquals(400, response.getStatusCode().value());
    }

    @Test
    void testEmptyDate() {
        ResponseEntity<NextWorkingDay> response = template.getForEntity("/next-working-day?after=", NextWorkingDay.class);
        Assert.assertEquals(400, response.getStatusCode().value());
    }
}
