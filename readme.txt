The application is created using SpringBoot and maven in Intellij IDE. Please execute mvn spring-boot:run to run the application.

The application has a controller class which exposes the /next-working-day API. It has a service class where the logic for getting the next working day is implemented. The test cases are added in DayApplicationTests.

The API can expected using /next-working-day, which will return the next working day

For example 
http://localhost:8080/next-working-day

will return
{
    "nextWorkingDay": "2019-12-04",
    "errorResponse": {
        "errorCode": 0,
        "errorMessage": "Success"
    }
}


If we need to get the next working day after any particular date then pass it with a paramenter with after. The input date should be in dd/MM/yyyy as defined in the Constants.java class. 

For example

http://localhost:8080/next-working-day?after=01/01/2019

will return 

{
    "nextWorkingDay": "2019-01-02",
    "errorResponse": {
        "errorCode": 0,
        "errorMessage": "Success"
    }
}

If the after paramenter is not valid (or not a valid date) then the API returns 400 error with an error code -100.

For example

http://localhost:8080/next-working-day?after=abc

will return

{
    "nextWorkingDay": null,
    "errorResponse": {
        "errorCode": -100,
        "errorMessage": "The after date is not a valid date"
    }
}

